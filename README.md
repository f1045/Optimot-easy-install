Easy install for the Optimot Ergo v1.5 keyboard layout. Linux only (Debian-based systems and Gnome are better supported).

See [bépoète.fr](https://www.xn--bpote-6rae.fr/optimot) and [Forum bépo](https://forum.bepo.fr/viewtopic.php?id=2049) for more information about Optimot.

## Installation

### Linux

Download the Linux *Optimot 1.5 Ergo* drivers from [bépoète.fr](https://www.xn--bpote-6rae.fr/optimot) and make the file (*Optimot_Linux_Ergo_15.zip*) available in the *X11/1.5 Ergo* directory.

To proceed with installing the drivers, from the repository root, run:
```
make
make install
```

`make install` will execute some commands using `sudo`. As a consequence, you may be prompted for your password.

## Uninstall

### Linux

From the repository root, run:
```
make uninstall
```

`make uninstall` will execute some commands using `sudo`. As a consequence, you may be prompted for your password.

## License

This project is not affiliated with the Optimot project.

Although the present automated installation procedure is open source (MIT licensed), the Optimot license is highly restrictive and does not allow distributing or modifying the layout, its drivers and documentation.
See the *LICENCE.rtf* file in the official documentation (*Doc-Optimot-1.5.zip* file).

As a consequence, using the Optimot layout is strongly discouraged.
