.PHONY: default

default:
	cd X11 && $(MAKE)

%:
	cd X11 && $(MAKE) $@
